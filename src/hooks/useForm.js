import { useState } from "react";

export default function useForm(formInitialValue= {}){

    const [formValue, setFormValue] = useState(formInitialValue);
    const [formFieldValidity, setFormFieldValidity] = useState({});
    const [formValidity,setFormValidity] = useState(false);

    const onChange = (evt) => {
        const identifiedAs = evt._targetInst.memoizedProps.identifiedAs;
        const fieldType = evt._targetInst.type;
        let newValue = null;

        if(fieldType === 'AndroidTextInput'){
            newValue = evt.nativeEvent.text;
        }else{
            if(fieldType === 'AndroidSwitch'){
                newValue = !formValue[identifiedAs];
            }
        }

        setFormValue(formValue => ({ ...formValue, [identifiedAs]: newValue }));

    }

    const isValidField = (formField)=>{

        return !Object.keys(formField).some(key =>{
            
            if(typeof formField[key] === 'object'){
                return isValidField(formField[key]);
            }else{
                return !formField[key];
            }

        });

    }

    const onBlur = (evt) =>{
        const regex = evt._targetInst.memoizedProps.regx;

        if(regex){
            const identifiedAs = evt._targetInst.memoizedProps.identifiedAs;
            const value = formValue[identifiedAs];
            const evaluation = {};

            Object.keys(regex).forEach(key => {
                if(key === 'eq' || key === 'equals'){
                    const compareTo = formValue[regex[key]];
                    evaluation[key] = value === compareTo;
                }else{   
                    evaluation[key] = new RegExp(regex[key],'g').test(value);
                }
            });

            setFormFieldValidity(formFieldValidity => ({...formFieldValidity, [identifiedAs] : evaluation}));
            setFormValidity(isValidField(formFieldValidity));
        }

    }

    return [
            formValue, 
            {
                onChange,
                onBlur
            },
            formFieldValidity,
            formValidity
        ];

}