import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';
import WelcomePage from '_pages/WelcomePage';
import IRestaurantNamePage from '_pages/IRestaurantNamePage';
import IRestaurantLogoPage from '_pages/IRestaurantLogoPage';
import IRestaurantDescriptionPage from '_pages/IRestaurantDescriptionPage';

export default createStackNavigator({
    WelcomePage: {
        screen: WelcomePage
    },
    IRestaurantName: {
        screen: IRestaurantNamePage
    },
    IRestaurantLogo: {
        screen: IRestaurantLogoPage
    },
    IRestaurantDescription: {
        screen: IRestaurantDescriptionPage
    }
},{
    defaultNavigationOptions: {
        header: null,
        ...TransitionPresets.SlideFromRightIOS
        
    }
});