import { createStackNavigator } from 'react-navigation-stack';
import SignInPage from '_pages/SignInPage';
import RestaurantWizardNavigator from '_navigations/RestaurantWizardNavigator';

export default createStackNavigator({
    SignIn: {
        screen: SignInPage,
        navigationOptions: {
            header: null
        }
    },
    RestaurantWizard: {
        screen: RestaurantWizardNavigator,
         navigationOptions : {
            header : null
        }
    }

});