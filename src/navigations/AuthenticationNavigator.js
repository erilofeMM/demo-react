import { createStackNavigator } from 'react-navigation-stack';
import LoginPage from '_pages/LoginPage';
import SignInNavigator from '_navigations/SignInNavigator';

export default createStackNavigator({
    Login: {
        screen: LoginPage,
        navigationOptions: {
            header: null
        }
    },
    SignIn: {
        screen: SignInNavigator,
        navigationOptions: {
            header: null
        }
    }
});