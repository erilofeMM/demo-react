import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import AuthenticationNavigator from '_navigations/AuthenticationNavigator';

const MainNavigator =  createSwitchNavigator({
    Authorization: {
        screen: AuthenticationNavigator,
        navigationOptions: {
            header: null
        }
    }
},{
    headerMode: 'none',
    mode: 'modal',
    transparentCard: true,
    cardStyle: { opacity: 1 }
  });


export default createAppContainer(MainNavigator);