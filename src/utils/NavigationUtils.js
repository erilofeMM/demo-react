import { NavigationActions } from 'react-navigation';

    let mainNavigatorRef;

    function setMainNavigator(ref){
        mainNavigatorRef = ref;
    }
    
    function navigate(routeName, params){

      mainNavigatorRef.dispatch(
        NavigationActions.navigate({
          routeName,
          params,
        })
      );
    }

    function goBack(){
      mainNavigatorRef.dispatch(
        NavigationActions.back()
      );
    }

export default {
    navigate,
    setMainNavigator,
    goBack
  };