import React from 'react';
import ButtonTitle from '_atoms/ButtonTitle';
import ActionButton from '_atoms/ActionButton';
export default function FormButton(props) {

    return(
        <ActionButton {...props}>
            <ButtonTitle
                textColor={props.textColor} 
                textAlign={props.textAlign} 
                text={props.children}/>
        </ActionButton>
    );

}