import React, { useState } from 'react';
import FormLabel from '_atoms/FormLabel';
import InputField from '_atoms/InputField';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { TouchableWithoutFeedback } from 'react-native';

export default function PasswordInputField(props){

    const [password, setPassword] = useState(true);

    return (
        <FormLabel>
            <InputField password={password}  {...props}/>
            {
                props.showHideSwitch && (
                    <TouchableWithoutFeedback onPress={()=>setPassword(password => !password)}>
                        <FontAwesomeIcon icon={password ? faEyeSlash : faEye}  size={28} style={{marginRight: 11}}/>
                    </TouchableWithoutFeedback>
                )
            }
        </FormLabel>
        );

}