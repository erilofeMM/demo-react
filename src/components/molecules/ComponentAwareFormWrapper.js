import React from 'react';
import { View, Switch } from 'react-native';
import FormInputField from '_molecules/FormInputField';
import PasswordInputField from '_molecules/PasswordInputField';
import InputField from '_atoms/InputField';

export default function ComponentAwareFormWrapper(props) {

    const inputFields = {};

    const addProps = ( childrens )=>{

        return React.Children.map(childrens, (child, i) => {

            let fieldProps = {
                ...props.fieldActions
            };

            if(props.formObject){
                fieldProps.value = props.formObject[child.props.identifiedAs];
            }

            if(child.type === FormInputField || child.type === PasswordInputField || child.type === InputField){

                fieldProps.inputRef = ref => (inputFields[child.props.identifiedAs] = ref);

                fieldProps.blurOnSubmit = (()=> {
                    let nextChild = childrens[i+1];
                    return !(nextChild 
                        && (nextChild.type === FormInputField 
                        || nextChild.type === PasswordInputField 
                        || nextChild.type === InputField))
                
                })();

                fieldProps.searchNext = () => {

                    let nextChild = childrens[i+1];

                    if(nextChild != undefined && (nextChild.type === FormInputField 
                            || nextChild.type === PasswordInputField 
                            || nextChild.type === InputField)){

                        inputFields[childrens[i+1].props.identifiedAs].focus();
                    }
                    
                }

            }

            return React.cloneElement(child, fieldProps);

        });

    }

    return (
        <View {...props}>
            {
                addProps(props.children)
            }
        </View>);

}