import React from 'react';
import FormLabel from '_atoms/FormLabel';
import InputField from '_atoms/InputField';
export default function FormInputField(props) {

    return (
        <FormLabel>
            <InputField {...props } />
        </FormLabel>
    );

}