import React from 'react';
import { SwitchContainer } from '_styles';
import { Switch, Text } from 'react-native';


export default function FormSwitch(props) {

    return (
        <SwitchContainer>
            <Switch 
                trackColor={{true: 'orange', false: 'grey'}}
                thumbTintColor='orange'
            {...props}/>
            <Text>{props.message}</Text>
        </SwitchContainer>
    );

}