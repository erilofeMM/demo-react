import React from 'react';
import { Label } from '_styles';

export default function FormLabel(props)  {
        
        return (<Label>{props.children}</Label>);

}