import React from 'react';
import { Animated } from 'react-native';
import { Picture } from '_styles';

export default function Logo(props) {
  
    return (
            <Picture 
                width={props.width}
                height={props.height}
                source={props.source}
                as={Animated.Image}/> 
    );

}