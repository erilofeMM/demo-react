import React from 'react';
import { Title } from '_styles';

export default function TextFormat(props) {
  
    return (
        <Title {...props}>{props.children}</Title> 
    );

}