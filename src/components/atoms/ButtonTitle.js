import { ButtonText } from '_styles';
import React from 'react';

export default function ButtonTitle(props) {

        return (
            <ButtonText color={props.textColor} align={props.textAlign}>
                {props.text}
            </ButtonText>
        );

}