import { BorderButton } from '_styles';
import React from 'react';
import { Link } from 'react-native';

export default function ActionButton(props) {
        
        return (       
            <BorderButton as={Link} {...props}>
                { props.children }
            </BorderButton>
        );

}