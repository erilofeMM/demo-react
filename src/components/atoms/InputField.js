import React from 'react';
import { Input } from '_styles';

export default function InputField(props) {

      return (
        <Input
            ref = {ref => props.inputRef(ref)}
            secureTextEntry={props.password}
            onSubmitEditing = {()=>{
                if(props.searchNext){
                    props.searchNext();
                }

                if(props.onSubmitEditing){
                    props.onSubmitEditing();
                }

              }
            }
            {... props} />
      )

    }