import React from 'react';
import  TextFormat from '_atoms/TextFormat';
import { View, Text } from 'react-native';
import FormButton from '_molecules/FormButton';
import NavigationUtils from '_utils/NavigationUtils';

export default function WelcomePage(props){

    const { params:userForm } = props.navigation.state;

    return (<>
        <View style={{flex: 3, justifyContent: 'flex-end'}}>
            <TextFormat H1 style={{textAlign:'center'}}>BIENVENIDO</TextFormat>
            <Text style={{textAlign:'center', width:350}}>
                El siguiente Wizard se encargará de guiarlo en los pasos requeridos para crear su restaurante, 
                una vez completado, podrá configurar sucursales, menús y horarios de disponibilidad 
                desde su apartado de configuracion.
            </Text>
        </View>
        <View style={{flex: 2, alignItems:'flex-end', justifyContent: 'space-around', flexDirection: 'row', borderBottomWidth:20, borderColor:'white'}}>
            <FormButton textAlign="center" textColor="white" onPress={NavigationUtils.goBack} warning>&lt;REGRESAR</FormButton>
            <FormButton textAlign="center" textColor="white" 
             onPress={ ()=> NavigationUtils.navigate('IRestaurantName',userForm) }
            warning>SIGUIENTE&gt;</FormButton>
        </View>
    </>);

}