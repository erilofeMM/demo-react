import React, { useEffect } from 'react';
import { View } from 'react-native';
import Logo from '_atoms/Logo';
import PasswordInputField from '_molecules/PasswordInputField';
import FormInputField from '_molecules/FormInputField'; 
import FormButton from '_molecules/FormButton';
import ComponentAwareFormWrapper from '_molecules/ComponentAwareFormWrapper';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import NavigationUtils from '_utils/NavigationUtils';
import useForm from '_hooks/useForm';

export default function LoginPage() {

    const [loginForm, bind] = useForm();
    const login = () => {
        console.log(
            `credentials= 
                [
                    email: ${loginForm.email}, 
                    password: ${loginForm.password}
                ]`
        );

    }

    return (
            <>
              <View style={{flex: 2, alignItems:'center', justifyContent: 'center', flexDirection: 'row'}}>
                <Logo />
              </View>
              <ComponentAwareFormWrapper style={{flex:1, justifyContent: 'center'}} fieldActions={bind} formObject={loginForm}>
                <FormInputField placeholder="Correo" identifiedAs="email" />
                <PasswordInputField  placeholder="Contraseña" identifiedAs="password" showHideSwitch/>
              </ComponentAwareFormWrapper>
              <View style={{flex: 2, justifyContent: 'center', alignItems:'center'}}>
              <FormButton textAlign="center" textColor="white" onPress={login} warning>LOGIN</FormButton>
                 <FormButton textAlign="center" textColor="black" onPress={ ()=> NavigationUtils.navigate('SignIn') } transparent>
                    Sign In instead
                    <FontAwesomeIcon icon={faChevronDown}  size={19} style={{marginRight: 11, color: 'silver'}}/>
                 </FormButton>
              </View>
            </>
    );

}