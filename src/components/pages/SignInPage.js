import React from 'react';
import { View } from 'react-native';
import Logo from '_atoms/Logo';
import PasswordInputField from '_molecules/PasswordInputField';
import FormInputField from '_molecules/FormInputField'; 
import FormButton from '_molecules/FormButton';
import ComponentAwareFormWrapper from '_molecules/ComponentAwareFormWrapper';
import FormSwitch from '_molecules/FormSwitch';
import useForm from '_hooks/useForm';
import NavigationUtils from '_utils/NavigationUtils';

export default function SignInPage(props) {

    const { params:userForm } = props.navigation?.state || {};

    console.log("FORM"+ JSON.stringify(userForm));

    const [signInForm,bind, signInInputFieldsValidity, isFormValid] = useForm(userForm);

    const basicCreateUser = () =>{
        console.log(JSON.stringify(signInForm));
    }

    return (
            <>
                <View style={{flex: 1, alignItems:'center', justifyContent: 'center', flexDirection: 'row'}}>
                    <Logo source={
                        !signInForm.restaurant 
                        ?   require('_assets/images/png/011-client.png') 
                        :   require('_assets/images/png/008-chef.png') }/>
                </View>
                <ComponentAwareFormWrapper style={{flex: 2, justifyContent: 'space-between'}} fieldActions={bind} formObject={signInForm}>
                    <FormInputField placeholder="Nombre completo" identifiedAs="fullname"
                        regx={{
                            format:"^[A-Za-zñáéíóú]+((\s)?([A-Za-zñáéíóú])+)*$"
                        }}/>
                    <FormInputField placeholder="Correo" identifiedAs="email" 
                        regx={{
                            format:"^\S+@\S+$"
                        }}/>
                    <PasswordInputField placeholder="Contraseña" identifiedAs="password" 
                        regx={{
                            minLength:"^.{8,}$"
                        }}
                        showHideSwitch/>
                    <PasswordInputField placeholder="Verificar contraseña" identifiedAs="passwordConfirmation" 
                        regx={{
                            eq : 'password'
                        }}
                        showHideSwitch/>
                    <FormSwitch identifiedAs="restaurant" 
                        message="Crear también mi perfil de restaurant"/>
                </ComponentAwareFormWrapper>
                <View style={{flex: 1, alignItems:'center', justifyContent: 'center', flexDirection: 'row'}}>
                    {
                        !signInForm.restaurant 
                        ? <FormButton textAlign="center" textColor="white" disabled={!isFormValid} onPress={basicCreateUser} >CREAR</FormButton>
                        : <FormButton 
                            textAlign="center" 
                            textColor="white" 
//                            disabled={!isFormValid}
                            onPress={ ()=> NavigationUtils.navigate('WelcomePage',signInForm) }>SIGUIENTE</FormButton>
                    }
                </View>
            </>
    );

}