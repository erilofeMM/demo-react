import React from 'react';
import useForm from '_hooks/useForm';
import  TextFormat from '_atoms/TextFormat';
import { View, Text } from 'react-native';
import FormButton from '_molecules/FormButton';
import ComponentAwareFormWrapper from '_molecules/ComponentAwareFormWrapper';
import FormInputField from '_molecules/FormInputField';
import NavigationUtils from '_utils/NavigationUtils';

export default function IRestaurantNamePage(props){

    const { params:userForm } = props.navigation.state;
    const [restaurantSignInForm,bind] = useForm(userForm);

    return (<>
        <View style={{flex: 1, justifyContent: 'center'}}>
            <TextFormat H1>PASO 1</TextFormat>
            <Text>Agrega un nombre para tu restaurant.</Text>
        </View>
        <ComponentAwareFormWrapper style={{flex: 1, justifyContent: 'flex-end'}} fieldActions={bind} formObject={restaurantSignInForm}>
            <FormInputField placeholder="restaurant" identifiedAs="restaurantName"/>
        </ComponentAwareFormWrapper>
        <View style={{flex: 2, alignItems:'flex-end', justifyContent: 'space-around', flexDirection: 'row', borderBottomWidth:20, borderColor:'white'}}>
            <FormButton textAlign="center" textColor="white" 
            onPress={()=> NavigationUtils.navigate('SignIn',userForm)} 
            warning>&lt;REGRESAR</FormButton>
            <FormButton textAlign="center" textColor="white" 
             onPress={ ()=> NavigationUtils.navigate('IRestaurantLogo',restaurantSignInForm) }
            warning>SIGUIENTE&gt;</FormButton>
        </View>
    </>);

}