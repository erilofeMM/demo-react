export { Palette } from '_styles/Palette';
export { Input } from '_styles/Input';
export { Picture } from '_styles/Picture';
export { Label } from '_styles/Label';
export { BorderButton, ButtonText } from '_styles/Button';
export { SwitchContainer } from '_styles/Switch';
export { Title } from '_styles/Text';

