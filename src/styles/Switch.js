import styled from 'styled-components/native';

export const SwitchContainer = styled.View`
    flex: 1;
    flex-direction: row;
    align-items: center;
`;