import styled, {css} from 'styled-components/native';

const size = css`${props => {

    let size = null;
    if(props.H1){
        size = 32;
    }else if(props.H2){
        size = 24;
    }else if(props.H3){
        size = 18.72
    }else if(props.H4){
        size = 16;
    }else if(props.H5){
        size = 13.28;
    }else if(props.H6){
        size = 10.72;
    }

    return size;

}}`;

export const Title = styled.Text`
    font-size: ${size}px;
    font-weight: bold;
    borderBottomWidth: 15; 
    borderColor: white;
`;