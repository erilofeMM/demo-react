import styled from 'styled-components/native';

export const Input = styled.TextInput.attrs(props => ({
    placeholder: props.placeholder ? props.placeholder : 'insert text here',
    secureTextEntry: props.secureTextEntry ? props.secureTextEntry : false
  }))`
  flex: 1;
  flex-direction: row;
  display: flex;
  align-items: center;
`;