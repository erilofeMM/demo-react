import styled, {css} from 'styled-components/native';
import { Dimensions } from 'react-native';


const { width, height } = Dimensions.get('window');


const color = css`${props => {

    let color = null;
    if(props.primary){
        color = props.theme.colors.primary;
    }else if(props.warning){
        color = props.theme.colors.warning;
    }else if(props.error){
        color = props.theme.colors.error;
    }else if(props.transparent){
        color = props.theme.colors.actions.invisible;
    }else {
        color = props.theme.colors.actions.disabled;
    }

    return color;

}}`;

export const BorderButton = styled.TouchableOpacity`
  display: inline-block;
  background-color: ${props => !props.border ? color : props.theme.colors.actions.invisible};
  font-size: 1px;
  margin: 1px;
  border: 1px solid ${color};
  padding: 0.25px 1px;
  border-radius: 10px;
  display: flex;
  width: ${props => props.width ? props.width : width * 0.29}px;
  height: ${props => props.height ? props.height : height * 0.1}px;
  justifyContent: center;
  alignItems: center;
`;

export const ButtonText = styled.Text`
    font-size: 15px;
    color: ${props => props.color};
    text-align: ${props => props.align};

`;