import styled from 'styled-components/native';

export const Label = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-width: 0.5px;
  height: 40px;
  border-radius: 5px;
  margin: 10px;
`;