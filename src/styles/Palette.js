export const Palette =  {
    colors:{
        primary: "blue",
        warning : "orange",
        error : "red",
        actions: {
            disabled: "silver",
            invisible: "transparent"
        }

    }
};