import styled from 'styled-components/native';
import { Dimensions } from 'react-native';


const { width, height } = Dimensions.get('window');

export const Picture = styled.Image.attrs(props => ({
    width: props.width ? props.width : '120px',
    height: props.height ? props.height : '120px',
    source: props.source ? props.source : require('_assets/images/png/023-menu.png')
  }))`
  width: ${props =>  props.width};
  height: ${props =>  props.height};
  border-radius: 15px;
  flex-direction: row;
  position: relative;
`;