import React, { Component } from 'react';
import {Palette} from '_styles';
import {ThemeProvider} from 'styled-components/native';
import MainNavigator from '_navigations';
import { View } from 'react-native';
import NavigationUtils from '_utils/NavigationUtils';

export default class App extends Component {  

  render() {
    console.disableYellowBox = true;
    return (
      <View style={{ flex: 1}}>
          <ThemeProvider theme={Palette}>
            <MainNavigator ref={ref => NavigationUtils.setMainNavigator(ref)}/>
          </ThemeProvider>
      </View>
    );

  }

}