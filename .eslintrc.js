module.exports = {
	root: true,
	extends: '@react-native-community',
	plugins: ['import'],
	settings: {
		  'import/resolver': {
			  node: {
				  paths: ['src'],
				  alias: {
					  _assets:"./src/assets",
					  _components:"./src/components",
					  _navigations:"./src/navigations",
					  _services:"./src/services",
					  _styles:"./src/styles",
					  _utils:"./src/utils",
					  _molecules : "./src/components/molecules",
					  _atoms : './src/components/atoms',
					  _pages : './src/components/pages',
					  _hooks : "./src/hooks"
				  },
			  },
		  },
	  }
  };
  